<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Repository\PromptRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PromptController extends AbstractController
{
    /**
     * Display all prompts.
     */
    #[Route('/prompt', name: 'app_prompt')]
    public function index(PromptRepository $promptRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $prompts = $paginator->paginate(
            $promptRepository->findAll(),
            $request->query->getInt('page', 1),
            10 /* limit per page */
        );

        return $this->render('pages/prompt/index.html.twig', [
            'prompts' => $prompts,
        ]);
    }
}
